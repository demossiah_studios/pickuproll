## Interface: 11305
## X-Min-Interface: 11302
## Title: PickUpRoll
## Author: DeMossiah
## Notes: Automatically distribute trash drops.
## SavedVariablesPerCharacter: PickUpRoll
## X-Curse-Project-ID: 409059
## Version: @project-version@

Localization/Locale.core.lua
Localization/Locale.deDE.lua
Localization/Locale.enUS.lua
Localization/Locale.esES.lua
Localization/Locale.esMX.lua
Localization/Locale.frFR.lua
Localization/Locale.itIT.lua
Localization/Locale.koKR.lua
Localization/Locale.ptBR.lua
Localization/Locale.ruRU.lua
Localization/Locale.zhCN.lua
Localization/Locale.zhTW.lua
Localization/Locale.lua

Core.lua
Command.lua
Panel.lua
PickUpRoll.lua
