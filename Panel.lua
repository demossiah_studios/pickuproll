local addonName, addon = ...

addon.panel = CreateFrame('FRAME')

--[[ Panel events ]]
addon.panel:RegisterEvent('ADDON_LOADED')
addon.panel:RegisterEvent('VARIABLES_LOADED')

addon.panel:SetScript('OnEvent', function(self, event, ...)
    if event == 'ADDON_LOADED' and ... == addonName then
        self:OnAddonLoaded()
    elseif event == 'VARIABLES_LOADED' then
        self:OnVariablesLoaded()
    end
end)

addon.panel:SetScript('OnShow', function(self)
    UIDropDownMenu_Initialize(self.aq20.dropdown, self.SetFrameMethodOnLoad)
    UIDropDownMenu_Initialize(self.aq40.dropdown, self.SetFrameMethodOnLoad)
    UIDropDownMenu_Initialize(self.bwl.dropdown, self.SetFrameMethodOnLoad)
    UIDropDownMenu_Initialize(self.mc.dropdown, self.SetFrameMethodOnLoad)
    UIDropDownMenu_Initialize(self.method.dropdown, self.SetFrameMethodOnLoad)
    UIDropDownMenu_Initialize(self.naxx.dropdown, self.SetFrameMethodOnLoad)
    UIDropDownMenu_Initialize(self.zg.dropdown, self.SetFrameMethodOnLoad)

    self.enabled:SetChecked(PickUpRoll.enabled)
    self.method:SetValue(PickUpRoll.method)

    self.boe:SetChecked(PickUpRoll.boe)
    self.materials:SetChecked(PickUpRoll.materials)

    self.bijous:SetChecked(PickUpRoll.bijous)
    self.coins:SetChecked(PickUpRoll.coins)
    self.idols:SetChecked(PickUpRoll.idols)
    self.keys:SetChecked(PickUpRoll.keys)
    self.scarabs:SetChecked(PickUpRoll.scarabs)

    self.bwl:SetValue(PickUpRoll.bwl)
    self.mc:SetValue(PickUpRoll.mc)
    self.naxx:SetValue(PickUpRoll.naxx)
    self.aq40:SetValue(PickUpRoll.aq40)

    self.aq20:SetValue(PickUpRoll.aq20)
    self.zg:SetValue(PickUpRoll.zg)
end)

--[[ Triggered when the addon is loaded ]]
function addon.panel:OnAddonLoaded()
    self.name = GetAddOnInfo(addonName)

    self:SetFrameTitle()
    self:SetFrameVersion()

    self:SetFrameCheckbox('enabled', addon:translate('Enable auto rolling'), self.title, 5, -32)

    self:SetFrameCheckbox('boe', addon:translate('Auto roll Bind on Equip'), self.enabled, 0, -32)
    self:SetFrameCheckbox('materials', addon:translate('Auto roll Materials'), self.boe)

    self:SetFrameCheckbox('bijous', addon:translate('Auto roll Bijous'), self.materials, 0, -32)
    self:SetFrameCheckbox('coins', addon:translate('Auto roll Coins'), self.bijous)
    self:SetFrameCheckbox('idols', addon:translate('Auto roll Idols'), self.coins)
    self:SetFrameCheckbox('keys', addon:translate('Auto roll Keys'), self.idols)
    self:SetFrameCheckbox('scarabs', addon:translate('Auto roll Scarabs'), self.keys)

    self:SetFrameMethod('method', addon:translate('Fallback method'), self.title, 250, -32)

    self:SetFrameMethod('bwl', addon:translate('Method in Blackwing Lair'), self.method, 0, -32)
    self:SetFrameMethod('mc', addon:translate('Method in Molten Core'), self.bwl)
    self:SetFrameMethod('naxx', addon:translate('Method in Naxxramas'), self.mc)
    self:SetFrameMethod('aq40', addon:translate("Method in Temple of Ahn'Qiraj"), self.naxx)

    self:SetFrameMethod('aq20', addon:translate("Method in Ruins of Ahn'Qiraj"), self.aq40, 0, -32)
    self:SetFrameMethod('zg', addon:translate("Method in Zul'Gurub"), self.aq20)

    self.cancel = function(this) this:OnInterfaceOptionsCancel() end
    self.default = function(this) this:OnInterfaceOptionsReset() end
    self.okay = function(this) this:OnInterfaceOptionsSubmit() end

    InterfaceOptions_AddCategory(self)
end

--[[ Triggered when the variables are loaded ]]
function addon.panel:OnVariablesLoaded()
    PickUpRoll = PickUpRoll or addon:GetDefaultVariables()
end

--[[ Triggered when the interface options cancel button is pressed ]]
function addon.panel:OnInterfaceOptionsCancel()
end

--[[ Triggered when the interface options reset button is pressed ]]
function addon.panel:OnInterfaceOptionsReset()
    addon.command:SetReset()
end

--[[ Triggered when the interface options okay button is pressed ]]
function addon.panel:OnInterfaceOptionsSubmit()
    PickUpRoll.enabled = self.enabled:GetChecked()
    PickUpRoll.method = self.method:GetValue()

    PickUpRoll.boe = self.boe:GetChecked()
    PickUpRoll.materials = self.materials:GetChecked()

    PickUpRoll.bijous = self.bijous:GetChecked()
    PickUpRoll.coins = self.coins:GetChecked()
    PickUpRoll.idols = self.idols:GetChecked()
    PickUpRoll.keys = self.keys:GetChecked()
    PickUpRoll.scarabs = self.scarabs:GetChecked()

    PickUpRoll.aq20 = self.aq20:GetValue()
    PickUpRoll.aq40 = self.aq40:GetValue()
    PickUpRoll.bwl = self.bwl:GetValue()
    PickUpRoll.mc = self.mc:GetValue()
    PickUpRoll.naxx = self.naxx:GetValue()
    PickUpRoll.zg = self.zg:GetValue()
end

--[[ Adds a checkbox to the panel ]]
function addon.panel:SetFrameCheckbox(setting, label, parent, offsetX, offsetY)
    self[setting] = CreateFrame('CheckButton', nil, self, 'UICheckButtonTemplate')

    self[setting]:SetChecked(PickUpRoll[setting])
    self[setting]:SetPoint('TOPLEFT', parent, 'BOTTOMLEFT', offsetX or 0, offsetY or 0)

    self[setting].text:SetText(label)
end

--[[ Adds the method dropdown to the panel ]]
function addon.panel:SetFrameMethod(setting, label, parent, offsetX, offsetY)
    local frame = CreateFrame('Frame', nil, self)

    frame:SetSize(200, 32)
    frame:SetPoint('TOPLEFT', parent, 'BOTTOMLEFT', offsetX or 0, offsetY or 0)

    frame.text = self:CreateFontString(nil, 'ARTWORK', 'GameFontNormalSmall')
    frame.text:SetPoint('LEFT', frame, 'LEFT')
    frame.text:SetText(label)

    frame.dropdown = CreateFrame('Frame', nil, frame, 'UIDropDownMenuTemplate')
    frame.dropdown:SetPoint('RIGHT', frame, 'RIGHT')

    function frame:GetValue()
        return UIDropDownMenu_GetSelectedValue(frame.dropdown)
    end

    function frame:SetValue(method)
        UIDropDownMenu_SetSelectedValue(frame.dropdown, method)
    end

    self[setting] = frame
end

--[[ Adds the method options to the dropdown ]]
function addon.panel:SetFrameMethodOnLoad()
    for key, value in pairs(addon.methods) do
        local option = UIDropDownMenu_CreateInfo()

        option.arg1 = key
        option.func = self:GetParent().SetValue
        option.text = value
        option.value = key

        UIDropDownMenu_AddButton(option)
    end
end

--[[ Adds the addon title to the panel ]]
function addon.panel:SetFrameTitle()
    self.title = self:CreateFontString(nil, 'OVERLAY', 'GameFontNormalLarge')

    self.title:SetPoint('TOPLEFT', 15, -15)
    self.title:SetText(GetAddOnInfo(addonName))
end

--[[ Adds the addon version to the panel ]]
function addon.panel:SetFrameVersion()
    self.version = self:CreateFontString(nil, 'OVERLAY', 'GameFontNormalSmall')

    self.version:SetTextColor(0.5, 0.5, 0.5)
    self.version:SetPoint('BOTTOMLEFT', self.title, 'BOTTOMRIGHT', 5, 1)
    self.version:SetText(GetAddOnMetadata(addonName, 'Version'))
end
