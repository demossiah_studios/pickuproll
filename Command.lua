local addonName, addon = ...

addon.command = CreateFrame('FRAME')

--[[ Panel events ]]
addon.command:RegisterEvent('ADDON_LOADED')

addon.command:SetScript('OnEvent', function(self, event, ...)
    if event == 'ADDON_LOADED' and ... == addonName then
        self:OnAddonLoaded()
    end
end)

--[[ Triggered when the addon is loaded ]]
function addon.command:OnAddonLoaded()
    SLASH_PUR1 = '/pur'

    function SlashCmdList.PUR(...)
        self:OnSlashCommand(...)
    end
end

--[[ Triggered when slash command is received ]]
function addon.command:OnSlashCommand(args)
    local command, params = strsplit(' ', (args or ''):lower(), 2)

    if command == 'enabled' then self:SetState('enabled', addon:translate('Status'))
    elseif command == 'method' then self:SetMethod('method', addon:translate('Fallback'))

    elseif command == 'boe' then self:SetState('boe', addon:translate('Bind on Equip'))
    elseif command == 'material' then self:SetState('materials', addon:translate('Materials'))

    elseif command == 'bijou' then self:SetState('enabled', addon:translate('Bijous'))
    elseif command == 'coin' then self:SetState('coins', addon:translate('Coins'))
    elseif command == 'idol' then self:SetState('idols', addon:translate('Idols'))
    elseif command == 'key' then self:SetState('keys', addon:translate('Keys'))
    elseif command == 'scarab' then self:SetState('scarabs', addon:translate('Scarabs'))

    elseif command == 'bwl' then self:SetMethod('bwl', addon:translate('Blackwing Lair'), params)
    elseif command == 'mc' then self:SetMethod('mc', addon:translate('Molten Core'), params)
    elseif command == 'naxx' then self:SetMethod('naxx', addon:translate('Naxxramas'), params)
    elseif command == 'aq40' then self:SetMethod('aq40', addon:translate("Temple of Ahn'Qiraj"), params)

    elseif command == 'aq20' then self:SetMethod('aq20', addon:translate("Ruins of Ahn'Qiraj"), params)
    elseif command == 'zg' then self:SetMethod('zg', addon:translate("Zul'Gurub"), params)

    elseif command == 'reset' then self:SetReset()
    elseif command == 'status' then self:SetStatus()

    else self:SetFallback() end

    addon.panel:GetScript('OnShow')(addon.panel)
end

--[[ Opens the addon interface options panel ]]
function addon.command:SetFallback()
    InterfaceOptionsFrame_OpenToCategory(addon.panel)
    InterfaceOptionsFrame_OpenToCategory(addon.panel)
end

--[[ Updates the distribution method ]]
function addon.command:SetMethod(setting, label, method)
    local content = addon:translate('Method not available')
    if not addon.methods[method] then return addon:SetMessage(content) end

    PickUpRoll[setting] = method
    addon:SetMessageMethod(label, PickUpRoll[setting])
end

--[[ Resets all settings ]]
function addon.command:SetReset()
    PickUpRoll = addon:GetDefaultVariables()

    local content = addon:translate('Settings have been reset')
    addon:SetMessage(content)
end

--[[ Toggles an active state ]]
function addon.command:SetState(setting, label)
    PickUpRoll[setting] = not PickUpRoll[setting] or true
    addon:SetMessageState(label, PickUpRoll[setting])
end

--[[ Shows the current settings ]]
function addon.command:SetStatus()
    addon:SetMessageState(addon:translate('Status'), PickUpRoll.enabled)
    addon:SetMessageMethod(addon:translate('Fallback'), PickUpRoll.method)

    addon:SetMessageState(addon:translate('Bind of Equip'), PickUpRoll.boe)
    addon:SetMessageState(addon:translate('Materials'), PickUpRoll.materials)

    addon:SetMessageState(addon:translate('Bijous'), PickUpRoll.bijous)
    addon:SetMessageState(addon:translate('Coins'), PickUpRoll.coins)
    addon:SetMessageState(addon:translate('Idols'), PickUpRoll.idols)
    addon:SetMessageState(addon:translate('Keys'), PickUpRoll.keys)
    addon:SetMessageState(addon:translate('Scarabs'), PickUpRoll.scarabs)

    addon:SetMessageMethod(addon:translate('Blackwing Lair'), PickUpRoll.bwl)
    addon:SetMessageMethod(addon:translate('Molten Core'), PickUpRoll.mc)
    addon:SetMessageMethod(addon:translate('Naxxramas'), PickUpRoll.naxx)
    addon:SetMessageMethod(addon:translate("Temple of Ahn'Qiraj"), PickUpRoll.aq40)

    addon:SetMessageMethod(addon:translate("Ruins of Ahn'Qiraj"), PickUpRoll.aq20)
    addon:SetMessageMethod(addon:translate("Zul'Gurub"), PickUpRoll.zg)
end
