local addonName, addon = ...

addon.frame = CreateFrame('FRAME')

--[[ Frame events ]]
addon.frame:RegisterEvent('ADDON_LOADED')
addon.frame:RegisterEvent('LOOT_OPENED')

addon.frame:SetScript('OnEvent', function(self, event, ...)
    if event == 'ADDON_LOADED' and ... == addonName then
        self:OnAddonLoaded()
    elseif event == 'LOOT_OPENED' then
        self:OnLootOpened()
    end
end)

--[[ Triggered when the addon is loaded ]]
function addon.frame:OnAddonLoaded()
    self.history = {}

    self.instances = {
        [309] = 'zg',
        [409] = 'mc',
        [469] = 'bwl',
        [509] = 'aq20',
        [531] = 'aq40',
        [533] = 'naxx',
    }

    self.items = {
        ['bijous'] = {
            19707, -- Red Hakkari Bijou
            19708, -- Blue Hakkari Bijou
            19709, -- Yellow Hakkari Bijou
            19710, -- Orange Hakkari Bijou
            19711, -- Green Hakkari Bijou
            19712, -- Purple Hakkari Bijou
            19713, -- Bronze Hakkari Bijou
            19714, -- Silver Hakkari Bijou
            19715, -- Gold Hakkari Bijou
        },

        ['coins'] = {
            19698, -- Zulian Coin
            19699, -- Razzashi Coin
            19700, -- Hakkari Coin
            19701, -- Gurubashi Coin
            19702, -- Vilebranch Coin
            19703, -- Witherbark Coin
            19704, -- Sandfury Coin
            19705, -- Skullsplitter Coin
            19706, -- Bloodscalp Coin
        },

        ['idols'] = {
            20866, -- Azure Idol
            20867, -- Onyx Idol
            20868, -- Lambent Idol
            20869, -- Amber Idol
            20870, -- Jasper Idol
            20871, -- Obsidian Idol
            20872, -- Vermillion Idol
            20873, -- Alabaster Idol
            20874, -- Idol of the Sun
            20875, -- Idol of Night
            20876, -- Idol of Death
            20877, -- Idol of the Sage
            20878, -- Idol of Rebirth
            20879, -- Idol of Life
            20881, -- Idol of Strife
            20882, -- Idol of War
        },

        ['keys'] = {
            21761, -- Scarab Coffer Key
            21762, -- Greater Scarab Coffer Key
        },

        ['scarabs'] = {
            20858, -- Stone Scarab
            20859, -- Gold Scarab
            20860, -- Silver Scarab
            20861, -- Bronze Scarab
            20862, -- Crystal Scarab
            20863, -- Clay Scarab
            20864, -- Bone Scarab
            20865, -- Ivory Scarab
        },
    }

    local content = addon:translate('Addon loaded, current status')
    addon:SetMessageState(content, PickUpRoll.enabled)
end

--[[ Triggered when the loot window is opened ]]
function addon.frame:OnLootOpened()
    if not PickUpRoll.enabled then return end

    local lootMethod, masterLooterPartyID = GetLootMethod()
    if lootMethod ~= 'master' or masterLooterPartyID ~= 0 then return end

    local method = self:GetMethod()

    for lootIndex = 1, GetNumLootItems() do
        local itemLink = GetLootSlotLink(lootIndex)
        if not self:IsRollable(itemLink or '') then return end

        if method == 'random' then
            self:SetItemRandom(itemLink, lootIndex)
        elseif method == 'rr' then
            self:SetItemRoundRobin(itemLink, lootIndex)
        elseif method == 'self' then
            self:SetItemSelf(itemLink, lootIndex)
        end
    end
end

--[[ Generates a candidate object ]]
function addon.frame:GetCandidate(groupIndex, unit)
    return { ['groupIndex'] = groupIndex, ['guid'] = UnitGUID(unit) }
end

--[[ Generates a eligible candidates ]]
function addon.frame:GetCandidates(lootIndex)
    local response = {}

    for groupIndex = 1, 40 do
        local name = GetMasterLootCandidate(lootIndex, groupIndex)
        local candidate = name and self:GetCandidate(groupIndex, name)

        if candidate then table.insert(response, candidate) end
    end

    return response
end

--[[ Retrieves the method for the current zone ]]
function addon.frame:GetMethod()
    local _, _, _, _, _, _, _, instanceMapId = GetInstanceInfo()
    local instance = self.instances[instanceMapId]

    return PickUpRoll[instance] or PickUpRoll.method
end

--[[ Checks if the item bind type is allowed to be distributed ]]
function addon.frame:IsBindType(itemLink)
    local _, _, _, _, _, _, _, _, _, _, _, _, _, bindType = GetItemInfo(itemLink)

    if bindType == 0 then return PickUpRoll.materials end
    if bindType == 2 then return PickUpRoll.boe end

    return false
end

--[[ Checks if the item is allowed to be distributed ]]
function addon.frame:IsRollable(itemLink)
    local itemString = string.match(itemLink, 'item:(%d*)')
    local itemId = tonumber(itemString)

    for category, items in pairs(self.items) do
        for _, item in ipairs(items) do
            if itemId == item then return PickUpRoll[category] end
        end
    end

    return self:IsBindType(itemLink)
end

--[[ Distribute an item to a player ]]
function addon.frame:SetItem(itemLink, lootIndex, candidate)
    local _, _, _, _, _, _, _, _, item = strsplit(':', itemLink)

    local _, _, _, _, _, name = GetPlayerInfoByGUID(candidate.guid)
    local content = addon:translate(':name received :item', { ['name'] = name, ['item'] = itemLink })

    GiveMasterLoot(lootIndex, candidate.groupIndex)
    addon:SetMessage(content)
end

--[[ Find a random player to give the item to ]]
function addon.frame:SetItemRandom(itemLink, lootIndex)
    local candidates = self:GetCandidates(lootIndex)

    local maximum = table.getn(candidates)
    if maximum == 0 then return end

    local index = math.random(1, maximum)
    local candidate = candidates[index]

    self:SetItem(itemLink, lootIndex, candidate)
end

--[[ Find the first player with the least amount of items received to give the item to ]]
function addon.frame:SetItemRoundRobin(itemLink, lootIndex)
    local candidates = self:GetCandidates(lootIndex)

    local candidate = nil
    local received = -1

    for _, value in ipairs(candidates) do
        local count = table.getn(self.history[value.guid] or {})

        if received == -1 or received > count then
            candidate = value
            received = count
        end
    end

    if not candidate then return end
    self:SetItem(itemLink, lootIndex, candidate)

    if not self.history[candidate.guid] then self.history[candidate.guid] = {} end
    table.insert(self.history[candidate.guid], item)
end

--[[ Give the item to yourself ]]
function addon.frame:SetItemSelf(itemLink, lootIndex)
    local guid = UnitGUID('player')
    local candidates = self:GetCandidates(lootIndex)

    for _, candidate in pairs(candidates) do
        if candidate.guid == guid then return self:SetItem(itemLink, lootIndex, candidate) end
    end
end
