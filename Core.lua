local addonName, addon = ...

addon.methods = {
    ['disabled'] = string.format('|cff9d9d9d%s|r', addon:translate('Disabled')),
    ['random'] = string.format('|cffa335ee%s|r', addon:translate('Random')),
    ['rr'] = string.format('|cff0070dd%s|r', addon:translate('Round Robin')),
    ['self'] = string.format('|cffff8000%s|r', addon:translate('Self')),
}

addon.states = {
    [false] = string.format('|cffff3f40%s|r', addon:translate('Disabled')),
    [true] = string.format('|cff3ce13f%s|r', addon:translate('Enabled')),
}

--[[ Returns the default settings ]]
function addon:GetDefaultVariables()
    return {
        ['enabled'] = true,
        ['method'] = 'disabled',

        ['boe'] = true,
        ['materials'] = true,

        ['bijous'] = true,
        ['coins'] = true,
        ['idols'] = true,
        ['keys'] = true,
        ['scarabs'] = true,

        ['bwl'] = 'self',
        ['mc'] = 'self',
        ['naxx'] = 'self',
        ['aq40'] = 'self',

        ['aq20'] = 'random',
        ['zg'] = 'random',
    }
end

--[[ Submits a message ]]
function addon:SetMessage(content)
    local name = GetAddOnInfo(addonName)
    local message = string.format('|cffff8000[|r|cff00ccff%s|r|cffff8000]|r |cffffff00%s|r', name, content)

    print(message)
end

--[[ Submits a method message ]]
function addon:SetMessageMethod(setting, method)
    local content = string.format('%s: %s', setting, addon.methods[method])
    self:SetMessage(content)
end

--[[ Submits a state message ]]
function addon:SetMessageState(setting, state)
    local content = string.format('%s: %s', setting, addon.states[state])
    self:SetMessage(content)
end
