local _, addon = ...

addon.locale = GetLocale()
addon.translations = addon.localization[addon.locale] or {}

--[[ Generate a translated string ]]
function addon:translate(message, replacements)
    local response = self.translations[message] or message

    for key, value in pairs(replacements or {}) do
        local replacement = string.format(':%s', key)
        response = response:gsub(replacement, value)
    end

    return response
end
